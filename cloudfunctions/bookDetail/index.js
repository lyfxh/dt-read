// 云函数入口文件
const cloud = require("wx-server-sdk");

cloud.init();

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext();

  cloud.updateConfig({
    env: wxContext.ENV == 'local' ? 'test-go5gy' : wxContext.ENV
  });

  const db = cloud.database();

  const { source_id, read_url } = event;
  const source = await db
    .collection("book_sources")
    .doc(source_id)
    .field({ detail: true, name: true })
    .get();
  if (source.data && source.data.detail) {
    const detail = await cloud.callFunction({
      name: "worm",
      data: {
        url: read_url,
        type: source.data.detail.type,
        result: source.data.detail.result
      }
    });
    if (!detail.result) {
      return { errMsg: "请求错误" };
    }
    detail.result.source_id = source_id;
    detail.result.source_name = source.data.name;
    return { errMsg: "ok", data: detail.result };
  } else {
    return { errMsg: "源错误" };
  }
};
